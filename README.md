# A SimplexNoise Simple example of use in Godot

Simplex-Noise is a variation of perlin-noise algorithm, that has a task to create a "noise" variation in a graph, those variation act like random but if you take a close attention to it will have a pattern.

This algorithm is used in any sort of wave-like, undulating material or texture like map-generation, fire effects, water and clouds found in various games.

Simplex-Noise is also a built-in class in godot game engine. This game was created under the pretext to learn and develop with godot.

## This Project

This project just show a beautiful variation in a horizon-like line. You can change the variables of the simplexNoise during the wave and see the difference between than.

```
a = restart with new random seed
s + (UP or Down) = Increases/Decreases the Octaves
d + (UP or Down) = Increases/Decreases the period
f + (UP or Down) = Increases/Decreases the persistence
spacebar = Stop/Unstop Progression 
```