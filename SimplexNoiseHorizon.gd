extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var noise
var horizon
var playing = true
var state = {}
onready var info_data = $Inform_data

# Called when the node enters the scene tree for the first time.
func _ready():
	horizon = []
	var mouse_pos = Vector2(0,0)
	horizon.append(Vector2(0,OS.window_size.y/2))
	VisualServer.set_default_clear_color(Color(0,0,0))
	Engine.set_target_fps(30)
	noise = OpenSimplexNoise.new()
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 100
	noise.persistence = 7
	state['seed'] = noise.seed
	state['octaves'] = noise.octaves
	state['period'] = noise.period
	state['persistence'] = noise.persistence
	info_data.change_noise(state)
	
	pass # Replace with function body.

func _draw():
	
	var n = noise.get_noise_2dv(horizon[-1])
	horizon.append(Vector2(horizon[-1].x+6, horizon[-1].y+n*10))
	
	if horizon[-1].x > OS.window_size.x:
		horizon = [Vector2(0, horizon[-1].y)]
	if 0>horizon[-1].y or horizon[-1].y>OS.window_size.y:
		horizon.append(Vector2(horizon[-1].x, OS.window_size.y/2))
	
	var b = horizon[-1]
	info_data.change_rt([n, b.x, b.y, len(horizon)])
	draw_horizon(horizon)
	pass
	
func _input(event):
	if event.is_action_pressed("ui_select"):
		playing = not playing
	
	state['seed'] = noise.seed
	state['octaves'] = noise.octaves
	state['period'] = noise.period
	state['persistence'] = noise.persistence
	info_data.change_noise(state)
	pass

	
func _process(delta):
	if playing:
		update()
	if Input.is_action_just_pressed("seed"):
		_ready()
	elif Input.is_action_pressed("octaves"):
		if Input.is_action_just_pressed("ui_up"):
			noise.octaves+=1
		if Input.is_action_just_pressed("ui_down"):
			noise.octaves-=1
	elif Input.is_action_pressed("period"):
		if Input.is_action_pressed("ui_up"):
			noise.period+=1
		if Input.is_action_pressed("ui_down"):
			noise.period-=1
	elif Input.is_action_pressed("persistence"):
		if Input.is_action_pressed("ui_up"):
			noise.persistence+=1
		if Input.is_action_pressed("ui_down"):
			noise.persistence-=1

func draw_horizon(points):
	for index in range(len(points)):
		if(index > 0):
			draw_line(points[index-1], points[index], Color(0, 0.8, 1), 2)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
