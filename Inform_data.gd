extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var rt_text
var rt_info
var noise_info
var noise_text




# Called when the node enters the scene tree for the first time.
func _ready():
	rt_info = $Rows/Row1/Panel/VBoxContainer/Update_info
	rt_text = "noise: %6.2f, last_point: (%07.2f, %06.2f), lenth: %03d"
	noise_info = $Rows/Row1/Panel/VBoxContainer/Noise_variables
	noise_text = "noise_vars: seed: {seed}, octaves: {octaves}, period: {period}, persistence: {persistence}"
	
	rt_info.text = rt_text
	noise_info.text = noise_text
	
func change_rt(data : Array):
	rt_info.text = rt_text % data
func change_noise(data : Dictionary):
	noise_info.text = noise_text.format(data)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
